import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Scanner;

/*
Program to process item descriptions from a wholesaler and perform some string manipulation to remove excess white space and item size descriptions that had been added on unnecessarily. Place shitty strings into "rawstrings.txt" and they will be output into "ProcessedStrings.txt" when the program completes.

Author: Taylor Cook
Date: 2018-08-02
*/


class RemoveGarbageFromDescriptions {

	private static ArrayList<String> finalResult = new ArrayList<>();

	public static void main(String[] args) throws Exception {
		String fileName = "rawstrings.txt";
		Path rawFile = FileSystems.getDefault().getPath(fileName);
		processFile(rawFile);
		printResults();
	}

	private static void processFile(Path path) throws Exception {
		Scanner sc = new Scanner(path);
		String input;
		int lineNumber = 0;
		while (sc.hasNextLine()) {
			input = sc.nextLine();
			lineNumber++;
			String result = processString(input, lineNumber);
			result = result.replaceAll("\\s+", " "); //remove excess whitespaced
			finalResult.add(result);
		}

	}

	private static String processString(String input, int lineNumber) {
		int indexToSplit = 0;
		StringBuilder sb = new StringBuilder(input);
		sb.reverse(); //reverse the string because the shit we don't want is at the end of the string
		String temp = sb.toString();
		char[] stringSplit = temp.toCharArray();
		for (int i = 3; i < stringSplit.length; i++) {
			/* start at character 4 from end of string because they all end in XX YY where XX is a numerical size and YY is a unit of measurement. we know for sure we want that shit gone so we start after it. */
			if (Character.isAlphabetic(stringSplit[i]) || stringSplit[i] == '%') { //if we hit a alphabetic character or a percent to handle cases like chocolate chips which are notated in % cocoa
				try {
					if (stringSplit[i - 1] == ' ' && (stringSplit[i] != 'O' && stringSplit[i + 1] != 'Z' && stringSplit[i + 2] != ' ')) { //if the character behind the first alphabetical character is a whitespace and the next two characters are not OZ, that's where we split. The OZ is to handle cases where the size was printed twice. Yikes.
						indexToSplit = temp.length() - i;
						break;
					} else if (Character.isDigit(stringSplit[i - 1])) { //if the previous character is a number, keep it because it is part of the string we want to keep, not a size.
						indexToSplit = temp.length() - (i - 1);
						break;
					} else if (Character.isAlphabetic(stringSplit[i + 1]) && Character.isAlphabetic(stringSplit[i + 2])) {
						indexToSplit = temp.length() - i;
						break;
					}
				} catch (Exception e) {
					System.out.println(lineNumber);
					i = stringSplit.length;
					return input;
				}
			}
		}
		return input.substring(0, indexToSplit);


	}

	private static void printResults() throws IOException {
		String fileName = "ProcessedStrings.txt";
		Path path = FileSystems.getDefault().getPath(fileName);
		Files.createFile(path);
		PrintStream fileStream = null;
		PrintStream ps_console = System.out;
		try {
			fileStream = new PrintStream(fileName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		System.setOut(fileStream);
		for (String finalResults : finalResult) {
			System.out.println(finalResults);
		}
		assert fileStream != null;
		fileStream.flush();
		fileStream.close();
	}

}
